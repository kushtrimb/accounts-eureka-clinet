package com.accounts.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountsController {


    @RequestMapping(value = "/accounts", method = RequestMethod.GET)
    public Account getAccount()
    {
        Account ac = new Account();
        ac.setId(1);
        ac.setName("Test Account111");

        return ac;
    }
}
